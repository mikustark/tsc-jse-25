package ru.tsc.karbainova.tm.repository;

import lombok.NonNull;
import ru.tsc.karbainova.tm.api.repository.IOwnerRepository;
import ru.tsc.karbainova.tm.model.*;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {

    @NonNull
    protected final List<E> entities = new ArrayList<>();

    @Override
    public void remove(@NonNull String userId, @NonNull E entity) {
        if (!userId.equals(entity.getUserId())) return;
        entities.remove(entity);
    }


    public void clear(@NonNull String userId) {
        findAll(userId).forEach(o -> entities.remove(o.getUserId()));
    }

    public List<E> findAll(@NonNull final String userId) {
        return entities.stream().filter(o -> userId.equals(o.getUserId()))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public List<E> findAll(@NonNull final String userId, Comparator<E> comparator) {
        return findAll(userId).stream().sorted(comparator).collect(Collectors.toList());
    }

    @Override
    public void add(@NonNull final String userId, E entity) {
        entity.setUserId(userId);
        entities.add(entity);
    }

    @NonNull
    public final Predicate<E> predicateById(@NonNull final String id) {
        return s -> id.equals(s.getId());
    }

    @Override
    public E findById(@NonNull final String userId, @NonNull final String id) {
        return entities.stream()
                .filter(predicateById(id))
                .findFirst().orElse(null);
    }

    @Override
    public E removeById(@NonNull final String userId, @NonNull final String id) {
        Optional.ofNullable(findById(userId, id)).ifPresent(this::remove);
        return null;
    }
}
