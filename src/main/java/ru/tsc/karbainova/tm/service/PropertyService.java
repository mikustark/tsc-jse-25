package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.ISaltSettings;
import ru.tsc.karbainova.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService, ISaltSettings {
    @NonNull
    private static final String APP_VERSION_DEFAULT = "";
    @NonNull
    private static final String APP_VERSION_KEY = "application.version";
    @NonNull
    private static final String DEVELOPER_EMAIL_KEY = "developer.email";
    @NonNull
    private static final String DEVELOPER_EMAIL_DEFAULT = "";
    @NonNull
    private static final String DEVELOPER_NAME_KEY = "developer.name";
    @NonNull
    private static final String DEVELOPER_NAME_DEFAULT = "";
    @NonNull
    private static final String FILE_NAME = "application.properties";
    @NonNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";
    @NonNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";
    @NonNull
    private static final String PASSWORD_SECRET_DEFAULT = "";
    @NonNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";
    @NonNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    private String getValue(String name, String defaultName) {
        @Nullable final String systemProperty = System.getProperty(name);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getProperty(name);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(name, defaultName);
    }

    @NonNull
    @Override
    public String getApplicationVersion() {
        return getValue(APP_VERSION_KEY, APP_VERSION_DEFAULT);
    }

    @NonNull
    @Override
    public String getDeveloperName() {
        return getValue(DEVELOPER_NAME_KEY, DEVELOPER_NAME_DEFAULT);
    }

    @NonNull
    @Override
    public String getDeveloperEmail() {
        return getValue(DEVELOPER_EMAIL_KEY, DEVELOPER_EMAIL_DEFAULT);
    }

    @NonNull
    @Override
    public Integer getPasswordIteration() {
        @Nullable final String systemProperty = System.getProperty(PASSWORD_ITERATION_KEY);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable final String environmentProperty = System.getProperty(PASSWORD_ITERATION_KEY);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        return Integer.parseInt(
                properties.getProperty(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT)
        );
    }

    @Override
    public @NonNull String getPasswordSecret() {
        return getValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }
}
