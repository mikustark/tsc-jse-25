package ru.tsc.karbainova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
public abstract class AbstractEntity implements Serializable {

    @Getter
    @Setter
    @NonNull
    private String id = UUID.randomUUID().toString();

}
